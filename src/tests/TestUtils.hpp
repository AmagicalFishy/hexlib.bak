#ifndef TESTUTILS
#define TESTUTILS

#include <Vector3.hpp>

// Compares two Godot vectors
bool vEquals(godot::Vector3 a, godot::Vector3 b);

#endif
