#include "catch.hpp"
#include "HexUtils.hpp"
#include "TestUtils.hpp"

#include <Vector3.hpp>

// Test hex_dist works as intended
SCENARIO("hex_dist is used to find the horizontal distance between two hexagonal coordinates") {
    WHEN("The coordinates are the same") {
        godot::Vector3 a = godot::Vector3(0, 0, 0);

        THEN("The distance returned is 0") {
            REQUIRE(hex_dist(a, a) == 0);
        }
    }

    AND_WHEN("The coordinates are near one another") {
        godot::Vector3 a = godot::Vector3(-3, 0, +3);
        godot::Vector3 b = godot::Vector3(1, 3, -1);

        THEN("The distance returned makes sense") {
            REQUIRE(hex_dist(a, b) == 4);
        }
    }

    AND_WHEN("The coordinates are far from one another") {
        godot::Vector3 a = godot::Vector3(-250, 0, +250);
        godot::Vector3 b = godot::Vector3(100, 0, -100);

        THEN("The distance returned makes sense") {
            REQUIRE(hex_dist(a, b) == 350);
        }
    }
    AND_WHEN("The coordinates are dissimilar") {
        godot::Vector3 a = godot::Vector3(-2, 0, 0);
        godot::Vector3 b = godot::Vector3(3, 0, -2);

        THEN("The distance returned makes sense") {
            REQUIRE(hex_dist(a, b) == 5);
        }
    }
}

// Test hex_round
SCENARIO("hex_round is used to determine the coordinates of the hexagon a floating point is in"){
    WHEN("Non-integer hexagon coordinates are given") {
        godot::Vector3 coord_1 = godot::Vector3(.5, 0, 0);
        godot::Vector3 coord_2 = godot::Vector3(.25, 0, 0);
        godot::Vector3 coord_3 = godot::Vector3(.75, 0, 0);

        THEN("hex_round correctly rounds them to the appropriate hexagon") {
            REQUIRE(vEquals(hex_round(coord_1), godot::Vector3(1, 0, 0)));
            REQUIRE(vEquals(hex_round(coord_2), godot::Vector3(0, 0, 0)));
            REQUIRE(vEquals(hex_round(coord_3), godot::Vector3(1, 0, 0)));
        }
    }
}

// Test the Hex struct.
SCENARIO("Hex is to be initialized") {
    WHEN("Hex is initialized with its default constructor") { 
        Hex hex;

        THEN("its TERRAIN_TYPE is 0 and its position is a Godot default vector") {
            REQUIRE(hex.TERRAIN_TYPE == 0);
            REQUIRE(hex.position == godot::Vector3());
        }
    }
    WHEN("Hex is initialized with a Godot vector and terrain type") {
        godot::Vector3 position = godot::Vector3(1, 2, 3);
        Hex hex(position, 1);

        // THEN("its TERRAIN_TYPE is 0 and its position reflects the vector in instantiation") {
        //     REQUIRE(hex.TERRAIN_TYPE==1);
        //     REQUIRE(vEquals(hex.position, position));
        // }
    }
}
