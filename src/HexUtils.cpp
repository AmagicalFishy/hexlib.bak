#include <cmath>
#include "HexUtils.hpp"
#include <Vector3.hpp>
#include <Array.hpp>

/** Structure which represents a single hexagonal-prism */
Hex::Hex() { TERRAIN_TYPE = 0; }
Hex::Hex(int x_, int y_, int z_) {
    TERRAIN_TYPE = 0;
    position = hex_to_grid(godot::Vector3(x_, y_, z_));
}
Hex::Hex(godot::Vector3 coord, int terrain_type) {
    TERRAIN_TYPE = terrain_type;
    position = hex_to_grid(coord);
}

/** Simple lerp function */
float lerp(float a, float b, float t) {
    return a + (b - a) * t;
}

/** Simple lerp function for hexagonal coordinates */
godot::Vector3 hex_lerp(godot::Vector3 a, godot::Vector3 b, float t) {
    return godot::Vector3(lerp(a.x, b.x, t), lerp(a.y, b.y, t), lerp(a.z, b.z, t));
}

/** Returns the number of hexagons separating two hexagons on a coordinate plane */
int hex_dist(godot::Vector3 a, godot::Vector3 b) {
    int az = -a.x - a.z;
    int bz = -b.x - b.z;

    return std::fmax(std::fmax(std::abs(a.x - b.x), std::abs(a.z - b.z)), std::abs(az - bz));
}

/** 
    Rounds a 3D coordinate of floats to integers, ensuring that rounding follows the constraint of 
    axial coordinates
*/
godot::Vector3 hex_round(godot::Vector3 coord) {
    int rx = std::round(coord.x);
    int ry = std::round(coord.z);
    int rz = std::round(-rx - ry);

    int x_diff = std::abs(rx - coord.x);
    int y_diff = std::abs(ry - coord.z);
    int z_diff = std::abs(rz - (-coord.x - coord.z));
    
    if (x_diff > y_diff && x_diff > z_diff) {
        rx = -ry - rx - ry;
    } else if (y_diff > z_diff) {
        ry = -rx + rx + ry;
    }

    return godot::Vector3(rx, round(coord.y), ry);
}

/** Converts hexagonal (axial) coordinates to regular cartesian grid coordinates */
godot::Vector3 hex_to_grid(godot::Vector3 coord) {
    float x = (std::sqrt(3)/2)*coord.x + std::sqrt(3)*coord.z;
    float z = 1.5*coord.x;

    return godot::Vector3(x, coord.y, z);
}
