#include <Godot.hpp>
#include <Node.hpp>
#include <Vector3.hpp>
#include <Array.hpp>
#include <Dictionary.hpp>
#include <String.hpp>

#include "HexArray.hpp"
#include "HexUtils.hpp"
#include "nArray.hpp"

/**
    Stores voxel information for the whole world.

    All of the coordinates used in this construction are in Axial Coordinates, as described in
    https://www.redblobgames.com/grids/hexagons/#coordinates-axial - The only reference to grid
    coordinates comes from the voxel itself, which stores its own coordinates.

*/
// Called by Godot
void HexArray::_init() { 
    int h = HSIZE;
    int v = VSIZE;
    for (int i=-h; i <= h; i++) {
        for (int j=-v; j <= v; j++) {
            for (int k=-h; k <= h; k++) {
                cells_.set(Hex(i, j, k), i, j, k);
            }
        }
    }
}

HexArray::HexArray() { }

void HexArray::set_cell(godot::Vector3 coord, int terrain_type) {
    cells_.set(Hex(coord, terrain_type), int(coord.x), int(coord.y), int(coord.z));
}

godot::Dictionary HexArray::get_cell(godot::Vector3 coord) {
    Hex hex = cells_(int(coord.x), int(coord.y), int(coord.z));
    godot::Dictionary hex_info;
    hex_info["position"] = hex.position;
    hex_info["terrain_type"] = hex.TERRAIN_TYPE;

    return hex_info;
}

godot::Array HexArray::get_neighbors(godot::Vector3 coord) {
    godot::Array neighbors;
    for (int i=-1; i < 2; ++i) { // For one y-value below, this y-value, and one y-value above
        neighbors.push_back(godot::Vector3(coord.x, coord.y + i, coord.z + 1));     // North
        neighbors.push_back(godot::Vector3(coord.x + 1, coord.y + i, coord.z));     // North-East
        neighbors.push_back(godot::Vector3(coord.x + 1, coord.y + i, coord.z - 1)); // South-East
        neighbors.push_back(godot::Vector3(coord.x, coord.y + i, coord.z - 1));     // South
        neighbors.push_back(godot::Vector3(coord.x - 1, coord.y + i, coord.z));     // South-West
        neighbors.push_back(godot::Vector3(coord.x - 1, coord.y + i, coord.z + 1)); // North-West
    }
    neighbors.push_back(godot::Vector3(coord.x, coord.y + 1, coord.z)); // Up
    neighbors.push_back(godot::Vector3(coord.x, coord.y - 1, coord.z)); // Down

    return neighbors;
}

godot::Vector3 HexArray::closest_vertical_neighbor(godot::Vector3 coord, int depth) {
    for (int i=0; i <= depth; ++i) {
        if (cells_(int(coord.x), int(coord.y + i), int(coord.z)).TERRAIN_TYPE != 0) {
            return godot::Vector3(coord.x, coord.y + i, coord.z);
        }
        if (cells_(int(coord.x), int(coord.y - i), int(coord.z)).TERRAIN_TYPE !=0) {
            return godot::Vector3(coord.x, coord.y - i, coord.z);
        }
    }

    return godot::Vector3();
}

godot::Array HexArray::hex_line(godot::Vector3 a, godot::Vector3 b) {
    int n = hex_dist(a, b);
    godot::Array results;
    results.push_back(hex_round(a));
    for (int i=1; i < n + 1; ++i) {
        godot::Vector3 step = hex_round(hex_lerp(a, b, float(1.0/n)*i));
        results.push_back(closest_vertical_neighbor(step, 5));
    }
    return results;
}

godot::Array HexArray::get_ring(godot::Vector3 center, int radius) {
    godot::Array ring_coordinates;
    ring_coordinates.push_back(godot::Vector3(center.x, center.y, center.z + radius));          // North
    ring_coordinates.push_back(godot::Vector3(center.x + radius, center.y, center.z));          // North-East
    ring_coordinates.push_back(godot::Vector3(center.x + radius, center.y, center.z - radius)); // South-East
    ring_coordinates.push_back(godot::Vector3(center.x, center.y, center.z - radius));          // South
    ring_coordinates.push_back(godot::Vector3(center.x - radius, center.y, center.z));          // South-West
    ring_coordinates.push_back(godot::Vector3(center.x - radius, center.y, center.z + radius)); // North-West

    godot::Array tmp_coordinates;
    for (int i=0; i < 6; ++i) {
        godot::Array line = hex_line(ring_coordinates[(6 + (i - 1)) % 6], ring_coordinates[i]);
        for (int i=0; i<line.size(); ++i) {
            godot::Vector3 vec = godot::Vector3(line[i]);
            if (cells_(vec.x, vec.y, vec.z).TERRAIN_TYPE != 0) {
                tmp_coordinates.push_back(line[i]);
            } else {
                tmp_coordinates.push_back(closest_vertical_neighbor(line[i], 5));
            }
        }
    }

    for (int i=0; i < tmp_coordinates.size(); ++i) {
    //for (godot::Vector3 coord: tmp_coordinates) {
        ring_coordinates.push_back(tmp_coordinates[i]);
    }

    return ring_coordinates;
}

void HexArray::_register_methods() {
    register_method("set_cell", &HexArray::set_cell);
    register_method("get_cell", &HexArray::get_cell);
    register_method("get_neighbors", &HexArray::get_neighbors);
    register_method("closest_vertical_neighbor", &HexArray::closest_vertical_neighbor);
    register_method("hex_line", &HexArray::hex_line);
    register_method("get_ring", &HexArray::get_ring);
}
