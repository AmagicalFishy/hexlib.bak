1. Clone this repository
2. Run ./compile.sh to compile the library and GDNative script
3. Place `HexArray.gdns` and `HexLib.gdnlib` into your `addons` folder in your root project folder
4. Place `libHexLib.so` into `addons/x11` folder
5. Autoload the `HexArray.gdns` file
