#ifndef NARRAY
#define NARRAY

#include <array>
#include <stdexcept>

/** 
 * 3D heap allocated array that allows for negative integers. All indices passed to the () 
 * operator are scaled by HSIZE and VSIZE respectively, so the bounds of the indices are, 
 * respectively, [-HSIZE, HSIZE] and [-VSIZE, VSIZE]
 *
 * @param T Object type being stored
 * @param HSIZE Half of the horizontal size of the array (defines the limit of both the 
 *        X and Z values) 
 * @param VSIZE Half of the vertical size of the array (defines the limit of the Y values)
 */
template <typename T, int HSIZE, int VSIZE> struct nArray { 
    private:
        std::array<T, (HSIZE*2 + 1)*(VSIZE*2 + 1)*(HSIZE*2 + 1)>* array_ = 
            new std::array<T, (HSIZE*2 + 1)*(VSIZE*2 + 1)*(HSIZE*2 + 1)>();
        
        /** 
         * Checks whether or not the index is in bounds. std::array.at() will check whether the 
         * arithmetic of all requested indices is in bounds, but this checks whether or not each
         * individual index is within bounds
         */
        void bounds_check_(int x, int y, int z) {
            if (abs(x) > HSIZE || abs(y) > VSIZE || abs(z) > HSIZE) {
                throw std::out_of_range("One of your indices is out of range");
            }
        }

    public:
        /**
         * Returns element in array, scaled appropriately (e.g. - If HSIZE = VSIZE = 50, then
         * array_(-50, -50, -50) is scaled to (0, 0, 0), giving array_[0]
         */
        T operator() (int x, int y, int z) {
            bounds_check_(x, y, z);
            return (*array_).at((z + HSIZE)*(HSIZE * VSIZE) + (y + VSIZE)*HSIZE + (x + HSIZE));
        }
        
        void set(T object, int x, int y, int z) {
            bounds_check_(x, y, z);
            (*array_).at((z + HSIZE)*(HSIZE * VSIZE) + (y + VSIZE)*HSIZE + (x + HSIZE)) = object;
        }
        ~nArray() { delete array_; }
};

#endif
