#ifndef HEXARRAY
#define HEXARRAY

#include <Godot.hpp>
#include <Node.hpp>
#include <Vector3.hpp>
#include <Array.hpp>
#include <Dictionary.hpp>

#include "HexUtils.hpp"
#include "nArray.hpp"

/**
    Stores voxel information for the whole world.

    All of the coordinates used in this construction are in Axial Coordinates, as described in
    https://www.redblobgames.com/grids/hexagons/#coordinates-axial - The only reference to grid
    coordinates comes from the voxel itself, which stores its own coordinates.

*/
// VSIZE and HSIZE must be even
const int HSIZE = 256; // 132 on each side of 0
const int VSIZE = 32; // 32 on each side of 0

class HexArray : public godot::Node {
    GODOT_CLASS(HexArray, godot::Node);

    private: 
        nArray<Hex, HSIZE, VSIZE> cells_;

    public: 
        HexArray();
        /** Called by Godot. Initializs all cells to default constructed Hex */
        void _init();

        /**
            Getter & Setter methods for hex tiles

            Set hex converts the hexagonal coordinates to grid coordinates so that we can work
            in hex coordinates w.o. having to convert back and forth
        */
        void set_cell(godot::Vector3 coord, int terrain_type);
        godot::Dictionary get_cell(godot::Vector3 coord);

        /**
            Returns a vector of coordinates of the 20 surrounding tiles

            @param coord The coordinate of the tile whose neighbors will be fetched
            @return A vector of coordinates representing the location of each of the tile's 
            neighbors
        */
        godot::Array get_neighbors(godot::Vector3 coord);

        /**
            Returns the coordinates of the closest vertical neighbor. This searches 10 spaces up, 
            10 space down, and if nothing is found, returns a generic default constructed vecotor

            @param coord The coordinate of the tile whose neighbors will be fetched
            @return A set of coordinates of the nearest neighbor
        */
        godot::Vector3 closest_vertical_neighbor(godot::Vector3 coord, int depth);


        /**
            Returns a vector of coordinates of hexagons constructing the line between one hexagon 
            and another

            @param a Coordinate of the first hexagon from which the line starts
            @param b Coordinate of the second hexagon at which the line ends
        */
        godot::Array hex_line(godot::Vector3 a, godot::Vector3 b);


        /**
            Returns a vector of coordinates forming a ring around the given tile's coordinates

            @param coord The coordinate of the center tile around which the ring is formed
            @param radius The radius of the ring
        */
        godot::Array get_ring(godot::Vector3 center, int radius);

        static void _register_methods();
};

#endif
