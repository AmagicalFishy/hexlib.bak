#ifndef HEXUTILS
#define HEXUTILS

#include <cmath>
#include <Vector3.hpp>
#include <Array.hpp>

/** Structure which represents a single hexagonal-prism */
struct Hex {
    public:
        godot::Vector3 position;
        int TERRAIN_TYPE;

        Hex();
        Hex(int x_, int y_, int z_);
        Hex(godot::Vector3 coord, int terrain_type);
};

/** Simple lerp function */
float lerp(float a, float b, float t);

/** Simple lerp function for hexagonal coordinates */
godot::Vector3 hex_lerp(godot::Vector3 a, godot::Vector3 b, float t);

/** Returns the number of hexagons separating two hexagons on a coordinate plane */
int hex_dist(godot::Vector3 a, godot::Vector3 b);

/** 
    Rounds a 3D coordinate of floats to integers, ensuring that rounding follows the constraint of 
    axial coordinates
*/
godot::Vector3 hex_round(godot::Vector3 coord);

/** Converts hexagonal (axial) coordinates to regular cartesian grid coordinates */
godot::Vector3 hex_to_grid(godot::Vector3 coord);

#endif
